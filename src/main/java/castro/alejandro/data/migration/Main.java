package castro.alejandro.data.migration;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import castro.alejandro.data.migration.model.NewTable;
import castro.alejandro.data.migration.model.OldTable;

public class Main {

    public static List<OldTable> diff(List<NewTable> newTableList, List<OldTable> oldTableList){
        Set<Object> storage = new HashSet<Object>();
        for (NewTable newTable : newTableList) {
            storage.add(newTable);
        }
        List<OldTable> result = new LinkedList<OldTable>();
        for (OldTable oldTable : oldTableList) {
            if(!storage.contains(oldTable)) {
                result.add(oldTable);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        List<OldTable> oldTable = new LinkedList<OldTable>();
        List<NewTable> newTable = new LinkedList<NewTable>();
        oldTable.add(new OldTable("1", "1", "1", "1","1", "migrado"));
        newTable.add(new NewTable("1", "1", "1", "1","1", "?", "migrado"));
        oldTable.add(new OldTable("2", "1", "1", "1","1", "migrado"));
        newTable.add(new NewTable("2", "1", "1", "1","1", "?", "migrado"));
        oldTable.add(new OldTable("1", "2", "1", "1","1", "sin migrar"));
        oldTable.add(new OldTable("1", "1", "2", "1","1", "migrado"));
        newTable.add(new NewTable("1", "1", "2", "1","1", "?", "migrado"));
        oldTable.add(new OldTable("2", "2", "1", "1","1", "migrado"));
        newTable.add(new NewTable("2", "2", "1", "1","1", "?", "migrado"));
        oldTable.add(new OldTable("1", "2", "1", "2","1", "sin migrar"));
        List<OldTable> result = diff(newTable, oldTable);
        for (OldTable resultItem : result) {
            System.out.println(resultItem.getOther());
        }
    }

}

package castro.alejandro.data.migration.model;

public class NewTable {

    public String pk1;
    public String pk2;
    public String pk3;
    public String pk4;
    public String pk5;
    public String pk6;
    public String other;

    public NewTable(String pk1, String pk2, String pk3, String pk4, String pk5, String pk6, String other) {
        super();
        this.pk1 = pk1;
        this.pk2 = pk2;
        this.pk3 = pk3;
        this.pk4 = pk4;
        this.pk5 = pk5;
        this.pk6 = pk6;
        this.other = other;
    }

    public String getPk1() {
        return pk1;
    }

    public void setPk1(String pk1) {
        this.pk1 = pk1;
    }

    public String getPk2() {
        return pk2;
    }

    public void setPk2(String pk2) {
        this.pk2 = pk2;
    }

    public String getPk3() {
        return pk3;
    }

    public void setPk3(String pk3) {
        this.pk3 = pk3;
    }

    public String getPk4() {
        return pk4;
    }

    public void setPk4(String pk4) {
        this.pk4 = pk4;
    }

    public String getPk5() {
        return pk5;
    }

    public void setPk5(String pk5) {
        this.pk5 = pk5;
    }

    public String getPk6() {
        return pk6;
    }

    public void setPk6(String pk6) {
        this.pk6 = pk6;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }


    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + pk1.hashCode();
        result = 31 * result + pk2.hashCode();
        result = 31 * result + pk3.hashCode();
        result = 31 * result + pk4.hashCode();
        result = 31 * result + pk5.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if ((other instanceof OldTable)) {
            OldTable oldTable = (OldTable) other;
            return this.pk1 == oldTable.pk1 &&
                    this.pk2 == oldTable.pk2 &&
                    this.pk3 == oldTable.pk3 &&
                    this.pk4 == oldTable.pk4 &&
                    this.pk5 == oldTable.pk5;
        }

        if ((other instanceof NewTable)) {
            NewTable newTable = (NewTable) other;
            return this.pk1 == newTable.pk1 &&
                    this.pk2 == newTable.pk2 &&
                    this.pk3 == newTable.pk3 &&
                    this.pk4 == newTable.pk4 &&
                    this.pk5 == newTable.pk5;
        }
        return false;
    }
}
